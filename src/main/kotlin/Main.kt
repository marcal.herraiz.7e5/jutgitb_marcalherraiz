import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*



class Problema(private val enunciat: String, JocDeProvesPublic: List<JocDeProves>, JocDeProvesPrivat: List<JocDeProves>, private val resolt: Boolean = false, private var intents: Int, private val llistaIntents: MutableList<Intent>){
    private val jocDeProvesPub = JocDeProvesPublic.random()
    private val jocDeProvesPriv = JocDeProvesPrivat.random()

    fun mostrarProblema(i: Int) {
        println("\n${i+1} - Problema\n" +
                enunciat +
                "\nEntrada - ${jocDeProvesPub.input}" +
                "\nSortida - ${jocDeProvesPub.output}")
    }

    fun comprovarProblema(sc: Scanner){
        print("\nEntrada - ${jocDeProvesPriv.input}" +
                "\nSortida: ")
        val sortidaUsuari = sc.next()
        intents++
        llistaIntents.add(Intent(sortidaUsuari, dataFormatejada))
        if (sortidaUsuari==jocDeProvesPriv.output){
            println("\n${GREEN}Resposta Correcta!!$RESET")
            !resolt
        }else{
            println("\n${RED}Resposta Incorrecta!!$RESET" +
                    "\n\nLlista d'Intents:")
            for (i in 0..llistaIntents.lastIndex){
                    println("Intent ${i+1} \"${llistaIntents[i].timestamp}\" Sortida - ${llistaIntents[i].sortida}")
                }
            if (resoldreProblema(sc, false)){
                   comprovarProblema(sc)
            }
        }
    }
}


class JocDeProves(val input: String, val output: String)

class Intent(val sortida: String, val timestamp: String)

const val RED = "\u001b[0;31m" // RED
const val GREEN = "\u001b[0;32m" // GREEN
const val RESET = "\u001B[0m" //TEXT RESET

val dataActual: LocalDateTime = LocalDateTime.now()
val format: DateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
val dataFormatejada = dataActual.format(format).toString()

fun main() {
    val sc = Scanner(System.`in`)
    val llistaProblemes = listOf(
        Problema("Escriu un programa que llegeixi un número per entrada i imprimeixi el doble del seu valor.", listOf(JocDeProves("2","4"),JocDeProves("16","32"),JocDeProves("8","16")), listOf(JocDeProves("10","20"),JocDeProves("50","100"),JocDeProves("186","372")), false,0, mutableListOf()),
        Problema("Escriu un programa que llegeixi un nombre enter i imprimeixi una frase amb el següent nombre enter.", listOf(JocDeProves("5","6"),JocDeProves("24","25"),JocDeProves("8","9")), listOf(JocDeProves("42","43"),JocDeProves("-15","-14"),JocDeProves("186","187")),false,0, mutableListOf()),
        Problema("Llegeix un valor amb decimals i imprimeix el doble.", listOf(JocDeProves("2.1","4.2"),JocDeProves("123.456","246.912"),JocDeProves("8.3","16.6")), listOf(JocDeProves("15.42","30.84"),JocDeProves("3.85","7.7"),JocDeProves("9.52","19.04")),false,0, mutableListOf()),
        Problema("L'usuari escriu un enter amb la seva edat i s'imprimeix true si és major d'edat, i false en qualsevol altre cas.", listOf(JocDeProves("16","false"),JocDeProves("18","true"),JocDeProves("45","true")), listOf(JocDeProves("12","false"),JocDeProves("76","true"),JocDeProves("19","true")),false,0, mutableListOf()),
        Problema("Fes un programa que rebi un caràcter i digui si és una lletra o no.", listOf(JocDeProves("B","true"),JocDeProves("G","true"),JocDeProves("?","false")), listOf(JocDeProves("2","false"),JocDeProves("K","true"),JocDeProves("$","false")),false,0, mutableListOf())
    )

    println("JutgITB")
    for (i in 0..llistaProblemes.lastIndex){
        llistaProblemes[i].mostrarProblema(i)
        if (resoldreProblema(sc,true)){
            llistaProblemes[i].comprovarProblema(sc)
        }
    }
}

fun resoldreProblema(sc: Scanner, primeraVegada: Boolean): Boolean {
    var resoldreProblema: String
    do {
        if (primeraVegada){
            print("\nVols resoldre el problema? [Sí / No]: ")
        }else{
            print("\nVols tornar a intentar resoldre el problema? [Sí / No]: ")
        }
        resoldreProblema = sc.next().lowercase()
    } while(resoldreProblema!="si" && resoldreProblema!="no" && resoldreProblema!="sí" )
    return resoldreProblema != "no"
}
